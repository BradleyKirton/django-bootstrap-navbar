from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from showcase.views import context_processor_view


urlpatterns = [
    path("admin/", admin.site.urls),
    path("", context_processor_view, name="home"),
    path("blog/", context_processor_view, name="blog"),
    path("examples/home/", context_processor_view, name="example-home"),
    path("examples/blog/", context_processor_view, name="example-blog"),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
