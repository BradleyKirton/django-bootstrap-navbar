from typing import Dict, Optional
from django.urls import reverse_lazy
from bootstrap_navbar.navbars.base import Href, LazyAttribute
from bootstrap_navbar.navbars.bootstrap4 import (
    NavBar,
    Brand,
    Link,
    Image,
    NavGroup,
    ListItem,
    DropDown,
)

import random


class LeftMenu(NavGroup):
    """Contains the navitems for the site pages."""

    user = ListItem(text="No Link")
    home = ListItem(
        text="Home", href=Href(url=reverse_lazy("home"), query_params={"field": 100})
    )
    blog = ListItem(text="Blog", href=reverse_lazy("blog"))
    pages = DropDown(
        text="Pages",
        children=[
            Link(text="Home", href=reverse_lazy("example-home")),
            Link(text="Blog", href=reverse_lazy("example-blog")),
        ],
        menu_class_list=["custom-class"],
        menu_attrs={"data-name": "pages-dropdown"},
    )
    documentation = LazyAttribute(method="get_documentation")

    class Meta:
        navitems = ("home", "blog", "user", "pages", "documentation")
        class_list = ["mr-auto"]

    def get_documentation(self, context: Dict) -> Optional[ListItem]:
        if not random.choice([True, False]):
            return None

        return ListItem(text="Documentation", href=reverse_lazy("blog"))


class RightMenu(NavGroup):
    """Contains the navitems for the account profile."""

    profile = DropDown(
        text="Accounts",
        children=[
            Link(text="Home", href=reverse_lazy("example-home")),
            Link(text="Blog", href=reverse_lazy("example-blog")),
        ],
    )

    class Meta:
        class_list = ["ml-auto"]
        navitems = ["profile"]


class ExampleNavBar(NavBar):
    """A NavBar object consists of nav groups and a brand."""

    left_menu = LeftMenu()
    right_menu = RightMenu()

    class Meta:
        brand = Brand(
            text="bootstrap-navbar",
            href="https://github.com/BradleyKirton/django-bootstrap-navbar",
            attrs={"target": "_"},
            image=Image(
                src=(
                    "https://getbootstrap.com/docs/4.0/"
                    "assets/brand/bootstrap-solid.svg"
                ),
                attrs={"width": 30, "height": 30},
                class_list=[
                    "d-inline-block",
                    "align-top",
                    "border",
                    "border-white",
                    "rounded",
                ],
            ),
        )
        attrs = {"style": "background-color: #563d82;"}
        navgroups = ("left_menu", "right_menu")
        class_list = ["navbar-dark"]
