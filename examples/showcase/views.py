from typing import Dict
from django.conf import settings
from pygments import highlight
from pygments.lexers import PythonLexer
from pygments.formatters import HtmlFormatter
from django.views.generic import TemplateView

import pathlib


class ContextProcessorView(TemplateView):
    template_name = "showcase/example_1.html"

    def get_context_data(self, **kwargs: Dict) -> Dict:
        context = super().get_context_data(**kwargs)
        base_path = pathlib.Path(settings.BASE_DIR)

        with open(base_path.joinpath("showcase/navbar.py")) as f:
            navbar_code = f.read()

        formatter = HtmlFormatter()
        context["styles"] = formatter.get_style_defs()
        context["navbar_code"] = highlight(navbar_code, PythonLexer(), formatter)
        return context


# View functions
context_processor_view = ContextProcessorView.as_view()
