from bootstrap_navbar.apps import BootstrapNavbarConfig


def test_app_name():
    assert BootstrapNavbarConfig.name == "bootstrap_navbar"
